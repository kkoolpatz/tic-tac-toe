

// Global variables begin here 

var canvas = document.getElementById("myCanvas");
var changeMarkButton = document.getElementById("changeMark");
var changePlayOrderButton = document.getElementById("changePlayOrder");
var ctx = canvas.getContext("2d");
canvas.width = Math.min(window.innerHeight, window.innerWidth) * 0.7;
canvas.height = canvas.width + 50;

var mouseX;
var mouseY;

// In-Game global Variables begin here

var gameGrid;
var gameMode = "HC";
var gridSize = Math.min(canvas.height, canvas.width);
var cellSize = gridSize / 3;
var unitSize = Number(cellSize / 4);
// console.log("global unitsize: " + unitSize);
var isHumanTurn = true;
var isX = true;
var gameRestart;

// Initial state of the Game
init();
function init() {
    
    
    gameGrid = [
        ["", "", ""],
        ["", "", ""],
        ["", "", ""]
    ];
    
    console.log("isHumanTurn: " + isHumanTurn + ", isX : " + isX);
    // gameGrid = [
    //     ["X", "O", "O"],
    //     ["O" , "", "" ],
    //     ["" , "X", "X"]
    // ];

    gameRestart = false;
    // console.log("init gridSize: " + gridSize);
    // console.log("init cellsize: " + cellSize);
    drawCanvas();
    setFirstPlayer();
    setMark();
}

// Event listeners

canvas.addEventListener("mouseup", mouseUp, false);
canvas.addEventListener("mousemove", mouseMove, false);
window.addEventListener('resize', drawCanvas, false);


function setMark() {
    if (isHumanTurn == true && changeMarkButton.innerHTML[8] == "O") {
        isX = true
        console.log("human will play as X");
    } else if (isHumanTurn == false && changeMarkButton.innerHTML[8] == "O") {
        isX = false
        console.log("computer plays first, as O");
    } else if (isHumanTurn == false && changeMarkButton.innerHTML[8] == "X") {
        isX = true
        console.log("computer plays first, as X");
    } else if (isHumanTurn == true && changeMarkButton.innerHTML[8] == "X") {
        isX = false
        console.log("human plays first, as O");
    }
}


function changeMark() {
    if (changeMarkButton.innerHTML[8] == "O"){
        isX = false;
        console.log("human will play as O");
        document.getElementById("changeMark").innerHTML = "Play as X";
    } else {
        isX = true
        console.log("human will play as X");
        changeMarkButton.innerHTML = "Play as O";
    }
    init();
   
}


function setFirstPlayer(){
    if (changePlayOrderButton.innerHTML[5] == "S") {
        console.log("Human Plays first");
        isHumanTurn = true;
        drawResult("You go first!")
    } else {
        console.log("Computer Plays first");
        isHumanTurn = false;
    }
}



function changePlayOrder() {
    var changePlayOrderButton = document.getElementById("changePlayOrder");
    if (changePlayOrderButton.innerHTML[5] == "S") {
        console.log("Computer Plays first");
        isHumanTurn = false;
        changePlayOrderButton.innerHTML = "Play First";
    } else {
        console.log("Human Plays first");
        isHumanTurn = true;
        changePlayOrderButton.innerHTML = "Play Second";
    }
    init();
}

// checks if game has ended 

function hasGameEnded() {
    for (row = 0; row < 3; row++) {
        if (gameGrid[row][0] == gameGrid[row][1]
            && gameGrid[row][2] == gameGrid[row][1]
            && gameGrid[row][2] != "") {
            drawVictoryLine(row, 0, row, 2)
            gameRestart = true
            drawResult("You lose. Click Game to Restart");
        } else if (gameGrid[0][row] == gameGrid[1][row]
            && gameGrid[2][row] == gameGrid[1][row]
            && gameGrid[2][row] != "") {
            drawVictoryLine(0, row, 2, row)
            drawResult("You lose. Click Game to Restart");
            gameRestart = true
        }
    }
    if(gameGrid[0][0] == gameGrid[1][1] 
        && gameGrid[1][1] == gameGrid[2][2]
        && gameGrid[1][1] != "") {
        drawVictoryLine(0, 0, 2, 2)
        gameRestart = true;
    } else if (gameGrid[0][2] == gameGrid[1][1]
        && gameGrid[1][1] == gameGrid[2][0]
        && gameGrid[1][1] != "") {
        drawVictoryLine(0, 2, 2, 0)
        gameRestart = true
    }
    if (gameRestart == false) {
        if ([...gameGrid[0], ...gameGrid[1], ...gameGrid[2]].includes("")) {
            // Game not finished yet. 
        } else {
            
            drawResult("It's a Draw! Click Game to Restart");
            gameRestart = true
        }
    }
    
}


// Plays the next turn for all players

function playTurn(row, col){
    if (row > 2 || col > 2) {
        console.log("invalid playturn: " + row + ", " + col);    
        return
    }
    
    console.log("playturn: "+ row +", "+ col);
    if (gameGrid[row][col] == "") {
        // console.log("valid move");
        if (isX === true) {
            // console.log("marking X" + row + ", " + col);
            drawX(row, col)
            isX = false;
        } else {
            drawO(row, col)
            // console.log("marking O" + row + ", " + col);
            isX = true;
        }
        hasGameEnded()
        if (gameMode == "HC"){
            if (isHumanTurn == true) {
                isHumanTurn = false;
            } else {
                isHumanTurn = true;
            }
        }
    } else {
        console.log("Invalid move: gameGrid[row][col]: " + gameGrid);
        
    }
}

function autoplay(){

    if (gameRestart == true){
        return
    } else if (isX == true) {
        var myMark = "X";
    } else 
        var myMark = "O";
    console.log("Computer to play:")
    return playTurn(...impossibleAI(...[gameGrid], myMark))
}


function mouseMove(e) {
    mouseX = e.pageX - canvas.offsetLeft;
    mouseY = e.pageY - canvas.offsetTop;
    // ctx.font = "10px Arial";
    // wipeCanvas(340, 0, canvas.width, canvas.height)
    // ctx.fillStyle = "black";
    // ctx.fillText(mouseX + " : " + mouseY, 350, 170);
    
}

function mouseUp(e) {
    mouseX = e.pageX - canvas.offsetLeft;
    mouseY = e.pageY - canvas.offsetTop;
    if (mouseX > gridSize 
        || mouseY > gridSize
        || mouseX < 0
        || mouseY < 0) {
        return
    }

    // console.log("MouseX: " + mouseX);
    // console.log("MouseY: " + mouseY);
    if (gameRestart == true) {
        init()
        console.log("Mouse reported game restarted");
        return
    }   
    // console.log("Mouse reported game ongoing");

    col = Math.trunc(mouseX / cellSize)
    row = Math.trunc(mouseY / cellSize)
    
    if (isHumanTurn === true)
        playTurn(row, col);
}

function draw() {
    
    if (isHumanTurn === false) {
        autoplay()
    }
}
setInterval(draw, 10);


