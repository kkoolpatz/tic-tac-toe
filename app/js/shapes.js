


// resize canvas 

function drawCanvas() {
    // console.log("resize event triggered")
    canvas.width = Math.min(window.innerHeight, window.innerWidth) * 0.7;
    canvas.height = canvas.width + 50;
    gridSize = Math.min(canvas.height, canvas.width);
    cellSize = gridSize / 3;
    unitSize = Number(cellSize / 4);
    // console.log("new canvas size: " + canvas.height + ", " + canvas.height)
    // console.log("wiping canvas")
    wipeAllCanvas()
    //sleep(5000)
    drawGrid(canvas.width, canvas.height)
    // sleep(1000)
    drawGameState();
}

// Draw Result 

function drawResult(text) {
    ctx.font = "18px Arial";
    var startY = Math.min(canvas.width, canvas.height) + 30;
    var startX = (startY - 30) / 2;
    wipeCanvas(0, startY -30, canvas.width, canvas.height)
    ctx.fillStyle = "rgb(172, 165, 148)";
    ctx.textAlign = "center"
    ctx.fillText(text, startX, startY );
    // drawRect(0, startY, canvas.width, canvas.height)    

}
// Draw the game grid

function drawGrid() {
    // console.log("drawing grid canvas size: " + canvas.height + ", " + canvas.height)
    drawRect(2, 2, gridSize -4, gridSize -4, 4)
    drawRect(cellSize, 2, cellSize , gridSize - 4, 4)
    drawRect(2, cellSize, gridSize -4, cellSize, 4)    
}

function drawGameState(){
    // console.log("Drawing Game state")
    console.log(gameGrid);
    for (var row = 0; row < 3; row++) {
        for (var col = 0; col < 3; col++) {
            if (gameGrid[row][col] == "X") {
                drawX(row, col);
            } else if (gameGrid[row][col] == "O") {
                drawO(row, col);
            }
        }
    }
}

// Draw the X shape

function drawX(row, col) {

    x = (col + 0.5) * cellSize;
    y = (row + 0.5) * cellSize;
    var offset = gridSize * 2 / 30
    ctx.beginPath();
    ctx.lineWidth = 8;
    ctx.moveTo(x - offset, y - offset);
    ctx.lineTo(x + offset, y + offset);
    ctx.moveTo(x + offset, y - offset);
    ctx.lineTo(x - offset, y + offset);
    ctx.strokeStyle = "rgb(255, 0, 0)";
    ctx.stroke();
    gameGrid[row][col] = "X"
}

// Draw the O shape

function drawO(row, col) {
    x = (col + 0.5) * cellSize;
    y = (row + 0.5) * cellSize;
    ctx.beginPath();
    ctx.lineWidth = 8;
    ctx.arc(x, y, gridSize * 2 / 30, 0, Math.PI * 2, false);
    ctx.strokeStyle = "rgb(0, 255, 0)";
    ctx.stroke();
    // ctx.fillStyle = "#0a4e64";
    ctx.fillStyle = "rgba(0,0,0,0)";
    ctx.fill();
    ctx.closePath();
    gameGrid[row][col] = "O"
}


function drawVictoryLine(startRow, startCol, endRow, endCol) {
    
    var offsetX = 0;
    var offsetY = 0;

    if (startRow != endRow) offsetY = gridSize/10;
    if (startCol > endCol) offsetX = -(gridSize / 10);
    if (startCol < endCol) offsetX = gridSize / 10;
    
    startX = ((startCol + 0.5) * cellSize) - offsetX;
    console.log("startX: " + startX)
    endX = ((endCol + 0.5) * cellSize) + offsetX;
    console.log("endX: " + endX)
    startY = ((startRow + 0.5) * cellSize) - offsetY;
    console.log("startY: " + startY)
    endY = ((endRow + 0.5) * cellSize) + offsetY;
    console.log("endY: " + endY)
    ctx.beginPath();
    ctx.lineWidth = 20;
    ctx.strokeStyle = "rgba(200, 220, 255, 0.75)";
    ctx.moveTo(startX, startY);
    ctx.lineTo(endX, endY);
    ctx.stroke();
    ctx.closePath();
}