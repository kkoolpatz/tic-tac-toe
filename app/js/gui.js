
// All functions defined here are generic graphical ui


// Clear canvas 

function wipeCanvas(startX, startY, endY, endY){
    ctx.clearRect(startX, startY, endY, endY);
}

function wipeAllCanvas() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

// Draw a rectangle 

function drawRect(startX, startY, lengthX, lengthY, thickness) {
    ctx.lineWidth = thickness;
    ctx.beginPath();
    ctx.rect(startX, startY, lengthX, lengthY);
    // ctx.fillStyle = "rgb(172, 165, 148)";
    // ctx.fill();
    ctx.strokeStyle = "rgb(172, 165, 148)";
    ctx.stroke();
    ctx.closePath();
}

function sleep(millis) {
    var date = new Date();
    var curDate = null;
    do { curDate = new Date(); }
    while (curDate - date < millis);
}